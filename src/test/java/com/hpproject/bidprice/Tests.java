package com.hpproject.bidprice;

import com.hpproject.bidprice.CountofItems.PartitionerClass;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;

import org.junit.Before;
import org.junit.Test;


//RunWith(MockitoJUnitRunner.class)
public class Tests {

    MapDriver <LongWritable, Text, WCclass, IntWritable> mapDriver;
    ReduceDriver<WCclass, IntWritable, Text, IntWritable> reduceDriver;
    MapReduceDriver<LongWritable, Text, WCclass, IntWritable, Text, IntWritable> mapReduceDriver;

    CountofItems.MapClass mapper = new CountofItems.MapClass();
    PartitionerClass partitioner = new PartitionerClass();
    CountofItems.ReduceClass reducer = new CountofItems.ReduceClass();

    @Before
    public void setUp() throws Exception{       
        mapDriver = new MapDriver<LongWritable, Text, WCclass, IntWritable>();
        mapDriver.setMapper(mapper);
        reduceDriver = new ReduceDriver<WCclass, IntWritable, Text, IntWritable>();
        reduceDriver.setReducer(reducer);
        mapReduceDriver = new MapReduceDriver<LongWritable, Text, WCclass, IntWritable, Text, IntWritable>();
        mapReduceDriver.setMapper(mapper);
        mapReduceDriver.setReducer(reducer);
        mapDriver.addCacheFile(new File("cache/city.en.txt").toURI());
        mapReduceDriver.addCacheFile(new File("cache/city.en.txt").toURI());
    }
    @Test
    // Test default Reducer
    public void testReducerDefault() throws Exception {
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(1));
        values.add(new IntWritable(1));
        values.add(new IntWritable(1));
        Text first = new Text("zibo");
        Text second = new Text("Windows 7");
        reduceDriver.withInput(new WCclass(first,second), values);
        reduceDriver.withOutput(new Text("zibo"), new IntWritable(3));
        reduceDriver.runTest();
    }

}
   
