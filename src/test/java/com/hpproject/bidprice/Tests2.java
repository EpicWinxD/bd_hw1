package com.hpproject.bidprice;

import com.hpproject.bidprice.CountofItems.PartitionerClass;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import static org.junit.Assert.assertEquals;

import org.junit.Test;


//RunWith(MockitoJUnitRunner.class)
public class Tests2 {

    MapDriver<LongWritable, Text, WCclass, IntWritable> mapDriver;
    ReduceDriver<WCclass, IntWritable, Text, IntWritable> reduceDriver;
    MapReduceDriver<LongWritable, Text, WCclass, IntWritable, Text, IntWritable> mapReduceDriver;
    CountofItems.MapClass mapper = new CountofItems.MapClass();
    PartitionerClass partitioner = new PartitionerClass();
    CountofItems.ReduceClass reducer = new CountofItems.ReduceClass();

    @Test
    //Test custom Partitioner
    public void testPartitionerDefault() throws Exception {
        Text first = new Text("zibo");
        Text second = new Text("Windows 7");
        WCclass wc = new WCclass(first, second);
        IntWritable value = new IntWritable(1);
        int numred = partitioner.getPartition(wc, value, 10);
        assertEquals(numred, 7);
    }
}