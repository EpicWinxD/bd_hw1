package com.hpproject.bidprice;
import com.hpproject.bidprice.CountofItems.PartitionerClass;
import java.io.File;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;

import org.junit.Before;
import org.junit.*;


//RunWith(MockitoJUnitRunner.class)
public class Tests3 {

    MapDriver <LongWritable, Text, WCclass, IntWritable> mapDriver;
    ReduceDriver<WCclass, IntWritable, Text, IntWritable> reduceDriver;
    MapReduceDriver<LongWritable, Text, WCclass, IntWritable, Text, IntWritable> mapReduceDriver;

    CountofItems.MapClass mapper = new CountofItems.MapClass();
    PartitionerClass partitioner = new PartitionerClass();
    CountofItems.ReduceClass reducer = new CountofItems.ReduceClass();

    @Before
    public void setUp() throws Exception{
        mapDriver = new MapDriver<LongWritable, Text, WCclass, IntWritable>();
        mapDriver.setMapper(mapper);
        reduceDriver = new ReduceDriver<WCclass, IntWritable, Text, IntWritable>();
        reduceDriver.setReducer(reducer);
        mapReduceDriver = new MapReduceDriver<LongWritable, Text, WCclass, IntWritable, Text, IntWritable>();
        mapReduceDriver.setMapper(mapper);
        mapReduceDriver.setReducer(reducer);
        mapDriver.addCacheFile(new File("cache/city.en.txt").toURI());
        mapReduceDriver.addCacheFile(new File("cache/city.en.txt").toURI());
    }
    @Test
    // Test default MapReducer
    public void testMapReducerDefault() throws Exception {
        String inputString = "ec2bbca486bfb5a1a7724daac66a394	20131027171900361	1	CBKHZB0EeBf	Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)	112.227.88.*	146	149	2	4d279e1fd14d57521c224cce7a94ce4d	88f0d52291cb99d7b04380261f5871f7	null	2545335729	728	90	OtherView	Na	4	12627	277	153	null	2261	10102,11423,10131,10063,11092";
        mapReduceDriver.withInput(new LongWritable(), new Text(inputString));
        mapReduceDriver.withOutput(new Text("zibo"), new IntWritable(1));
        mapReduceDriver.runTest();
    }
}