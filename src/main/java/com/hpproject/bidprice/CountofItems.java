package com.hpproject.bidprice;

import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.net.URI;
import java.util.Map;
import java.util.HashMap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.mapreduce.Partitioner;
import eu.bitwalker.useragentutils.*;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


public class CountofItems {

    public static class MapClass extends Mapper<LongWritable, Text, WCclass, IntWritable> {

        private final static IntWritable ONE = new IntWritable(1);
        private final static IntWritable ZERO = new IntWritable(0);
        private WCclass item = new WCclass();
        private Map<Integer, String> cities = new HashMap<Integer, String>();

        @Override
        protected void setup(Context context) throws IOException {//first method which called when a node is setup to perfom map task
            
            Path[] citiesfiles = DistributedCache.getLocalCacheFiles(context.getConfiguration());
            if (citiesfiles != null && citiesfiles.length > 0) {
                for (Path citiesfile : citiesfiles) {
                    readFile(citiesfile);
                }
            }
        }

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            
            String[] parts = value.toString().split("\t");
            String osname = UserAgent.parseUserAgentString(parts[4]).getOperatingSystem().getName();
            int cityID = Integer.parseInt(parts[7]);
            int biddingPrice = Integer.parseInt(parts[19]);
            if (cities.containsKey(cityID)) {
                item.set(new Text(cities.get(cityID)), new Text(osname));
                if(biddingPrice > 250)                    
                    context.write(item, ONE);
                else
                    context.write(item, ZERO);
            }
                
        }

        private void readFile(Path filePath) throws IOException {

            BufferedReader bufread = new BufferedReader(new FileReader(filePath.toString()));
            String line;
            while ((line = bufread.readLine()) != null) {
                String[] parts = line.split("\t");
                cities.put(Integer.parseInt(parts[0]), parts[1]);
            }
        }
    }

    public static class PartitionerClass extends Partitioner<WCclass, IntWritable> {

        @Override
        public int getPartition(WCclass key, IntWritable value, int numReduceTasks) {
                return (key.getOsname().hashCode() & Integer.MAX_VALUE) % numReduceTasks;
        }

    }

    public static class ReduceClass extends Reducer<WCclass, IntWritable, Text, IntWritable> {

        private static Text cityname = new Text();
        private static IntWritable count = new IntWritable();
     
        @Override
        protected void reduce(WCclass key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable value : values) {
                sum += value.get();
            }
            cityname.set(key.getCity());
            count.set(sum);
            context.write(cityname, count);//for each reducer task will be created output file
        }
    }

    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        Job job = new Job(conf);
        job.setJobName("CountofItems");
        job.setJarByClass(CountofItems.class);
        DistributedCache.addCacheFile(new URI("hdfs://localhost:9000/cache/city.en.txt"), job.getConfiguration());
        job.setMapperClass(MapClass.class);
        job.setMapOutputKeyClass(WCclass.class);
        job.setMapOutputValueClass(IntWritable.class);
 
        job.setPartitionerClass(PartitionerClass.class);
        job.setReducerClass(ReduceClass.class);
 
        FileSystem hdfs = FileSystem.get(URI.create("hdfs://localhost:9000"), conf);
        FileStatus[] fileStatus = hdfs.listStatus(new Path("hdfs://localhost:9000/input")); // allows get files in hdfs
        for (FileStatus status : fileStatus) {
            Path inputPath = new Path(status.getPath().toString());
            MultipleInputs.addInputPath(job, inputPath, TextInputFormat.class, MapClass.class);
        }

        Path outPath = new Path("hdfs://localhost:9000/output");
        FileOutputFormat.setOutputPath(job, outPath);

        if (hdfs.exists(outPath)) {
            hdfs.delete(outPath, true);
        }
        job.setOutputKeyClass(Text.class); // out keys are cities
        job.setOutputValueClass(IntWritable.class); // values are counts
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        job.setNumReduceTasks(10);
        
        int result = job.waitForCompletion(true) ? 0 : 1;
        if (job.isSuccessful()) {
            System.out.println("Job was successful " + result);
        } else if (!job.isSuccessful()) {
            System.out.println("Job was not successful " + result);
        }
    }
}
