# Homework 1

DS:BDA Homework 1

### Prerequisites
- 64-bit VMs require a 64-bit host OS and a virtualization product that can support a 64-bit guest OS.
- The amount of RAM required by the VM: 8+ GiB 

### MANUAL

Build project: 
```sh
$ git clone git clone git clone https://EpicWinxD@bitbucket.org/EpicWinxD/bd_hw1.git
$ cd ./bid
```
Run:
```sh
$ ./go.sh
```
Show output in HDFS:
```sh
$ hdfs dfs -text /output/*
```


More information in [here](Report_hw1.docx)