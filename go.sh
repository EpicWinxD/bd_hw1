#!/bin/bash
export h=/home/hadoop
cd $h
$h/sbin/start-dfs.sh
$h/sbin/start-yarn.sh
hdfs dfs -rm -R /tmp_files /input /cache /output
rm -fR $h/project/bid/bidprice/target
rm -fR $h/result/*


hdfs dfs -mkdir /output
hdfs dfs -mkdir /tmp_files
hdfs dfs -mkdir /input
hdfs dfs -mkdir /cache
hdfs dfs -put $h/project/bid/bidprice/cache/* /cache

for i in 19 20 21 22 23 24 25 26 27
do
/home/hadoop/bin/hdfs dfs -copyFromLocal $h/project/bid/bidprice/input/imp.201310$i.txt.bz2 /tmp_files
hadoop fs -cat /tmp_files/imp.201310$i.txt.bz2 | bzip2 -d | hadoop fs -put - /input/imp.201310$i.txt
done

cd $h/project/bid/bidprice

time mvn compile
time mvn test
time mvn package

hadoop jar ./target/bidprice-1-jar-with-dependencies.jar CountofItems
cd $h

hdfs dfs -get /output/part-r-00* $h/result
hdfs dfs -text /output/part-r-00009 | head -n 5
hdfs dfs -text /output/part-r-00005 | head -n 10
exit 0
